Bab 1 : Pengantar Belajar Pemrograman Linux
- Apakah nama lisensi yang digunakan Free Software Foundation untuk membebaskan distribusi pemakaian software untuk umum ?
    - Lisensi tersebut adalah GPL (General Public License).
- Sebutkan empat jenis kebebasan dalam filosofi Perangkat Lunak Bebas
    - Bahwa kebebasan tersebut mengacu pada empat jenis kebebasan bagi para pengguna perangkat lunak
        - Kebebasan menjalankan programnya untuk tujuan apa aja
        - Kebebasan untuk mempelajari bagaimana program itu bekerja serta disesuaikan dengan kebutuhan
        - Kebebasan untuk menyebarluaskan kembali hasil salinan perangkat lunak
        - Kebebasan untuk meningkatkan kinerja program dan dapat menyebarkannya ke khalayak umum sehingga semua menikmati keuntungannya.
- Linux dapat berinteraksi dengan sistem operasi lain dengan 3 cara
    - kompabilitas file dan filesystem
    - kompabilitas network
    - emulasi (simulasi) sistem operasi
- Salah satu alasan menyebabkan akses virus terhadap linux sangat kecil.
    - Linux mewarisi tradisi Unix dengan mendukung adanya file permissions (izin file) yang dapat mencegah pengubahan atau penghapusan file tanpa izin dari pemiliknya. 



Bab 2 : Mengenal Shell Linux
- Apakah shell itu
    - shell adalah program penerjemah perintah yang menjembatani pengguna komputer dengan sistem operasi
- Apakah bedanya shell dengan kernel dan bagaimana hubungannya ?
    - Kernel merupakan inti dari sistem operasi. Jadi, lebih tepat kalau dikatakan bahwa shell menghubungkan user dengan kernel. Selanjutnya setiap perintah diajukan oleh user akan diterima oleh shell, diterjemahkan dan kemudian hasilnya dikirimkan ke kernell. Kernel melakukan operasi sesuai dengn perintah yang diminta user. 
- Karakteristik BASH
    - BASH merupakan jenis shell yang paling banyak digunakan. Bash ini merupakan pengembangan dari sh dan ksh UNIX.
    - Mayoritas distribusi linux saat ini menggunakan bash sebagai default shell-nya.
    - BASH dikembangkan di Free Software Foundation sehingga menjadi freeware shell.
    


Bab 3 : Shell Script dan Editor Teks
- shell script = kelompok dari perintah-perintah shell



Script list
- date (info tanggal)
- expr (expression)
- env (list env)
- echo
- Files and directories
    - pwd (working directory)
    - mv (move)
    - cd (change directory)
    - rm (remove)
    - ls (list directory)
    - cp (copy)
    - files
        - touch (buat file)
        - cat (baca file)
        - more (more files)
        - tail -f(follow)
        - ln (links)
        - head (read from first line)
- utilities
    - whoami (username)
    - man (doc package)
    - ps (process)
    - which (locate utility)
- search
    - grep (mencari string)
    - find (mencari file)
    - xargs (piping argumen)
- compress and sort
    - tar
    - gzip
- user management and permission
    - useradd (tambah user)
    - adduser
    - passwd (pasang kata sandi)
    - su <user>
    - whoami
    - userdel (hapus user) -r / remove full
    - chown (merubah permission file) - r(recursive)
    - chmod u+x (add permissions) vs chmod -g-r 
- networking Linux
    - ping
    - hostname (hostname)
    - ip 
    - nslookup
    - ifconfig
    - traceroute
    - whois 
    - ssh (over tcp, port 22)
    - curl 
    - wget